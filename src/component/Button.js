
const buttonStyle = {
  width: "70px",
  height: "70px",
  borderRadius: "50%",
  color: "#fff",
  backgroundColor: "#FFA500",
  fontSize: 30,
  border: "none",
  margin: "0 10px",
};
const Button = ({ number }) => {
  return <button style={buttonStyle}>{number}</button>;
};

export default Button;
